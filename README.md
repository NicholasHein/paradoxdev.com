# paradoxdev.com

### Build Pages
```bash
$ hugo
```

### Build Pages (include drafts)
```bash
$ hugo -D
```

### Test Locally
```bash
$ hugo server
```

### Create New Page
```bash
$ hugo new <address_relative_to_"/content">
```

### Hard Clean
```bash
$ rm -rf ./public
```
