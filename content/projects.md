---
title: "Projects"
date: 2020-06-27T13:08:31-05:00
draft: false
---

My public repositories (including this website) are hosted on GitLab.

*Note:* This page is currently a work-in-progress and does not represent even
close to the magnitude of the complex, weird, and fun projects I have worked on.

## Games
 - [Space Traveler](https://www.sites.google.com/site/openoceansoftware/space-traveler):
    an 80s arcade-style game built for the FBLA game and simulation programming
    competition using C# and system libraries (2017)

## AVR
 - [Tripwire Trap](https://gitlab.com/NicholasHein/tripwire-trap):
    an ATtiny85 based laser tripwire that lights a distant fire-cracker fuse
    used to ward off intruders (2019)
 - [High-Score Breathalyzer](https://gitlab.com/NicholasHein/High-Score-Breathalyzer):
    an ATmega382p based breathalyzer that makes it fun to monitor your drinking and increase awareness to decrease risk of alcohol-poisoning (2017)

## Web
 - [ParadoxDev.com](https://gitlab.com/NicholasHein/paradoxdev.com):
    a Hugo-based static website with DNS, SSL, and hosted on GitLab Pages (2020)

## Works-In-Progress
 - [Untitled Game](https://gitlab.com/NicholasHein/untitled-game):
    a multi-platform 2D game + engine writen in C (2020)
