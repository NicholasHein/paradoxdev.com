---
title: "Development Log"
date: 2021-05-03T23:31:06-05:00
draft: false
---

I work on a few personal projects in my free-time.  There is a lot of range in the kinds of projects I work on from hardware design to embedded systems development to this very website!  Each one has a lot of character, and I do my best to document my progress here.

To see all of my projects -- even the ones that don't have articles -- check out my [GitLab](//gitlab.com/NicholasHein).

## Projects
  - [Paradox Dev Website](#paradox-dev-website)
  - [Untitled Drone](#untitled-drone)
  - [Untitled Game](#untitled-game)


---


### Paradox Dev Website

This is pretty self explanatory.  It's this website!

  - **Progress:** *Live*
  - **Status:** *Maintenance* - working to improve and add content

**Articles**

I don't currently have anything here yet, but I plan on writing at least a tutorial on how to create a Hugo static site using GitLab pages.


---


### Untitled Drone

This project is one that I've meant to work on for a while.  In essence, I'm trying to get a hexacopter flying with hardware and software I've written from scratch.  However, to do this, I'm creating a standard framework for real-time drone systems.

  - **Progress:** *Development* - currently in the planning phase
  - **Status:** *In-Progress*

#### Articles

I don't have anything here yet, but once I'm done with the planning phase, I plan on doing a write-up on the plan, design, and requirements.


---


### Untitled Game

This project is a game written in C on top of my own custom game engine.  This is a collaborative project I'm working on with two friends from high school.  It's made to be an 80's-style arcade game.

  - **Progress:** *Development* - currently in partial redesign
  - **Status:** *Halted* - school is in progress, so it's not feasible to collaborate right now

#### Articles

  1. [Introduction](./untitled-game/introduction)
