---
title: "XDG"
date: 2021-05-03T23:06:06-05:00
draft: true
---

**XDG** is a helpful tool for standardizing files, directories, and communication with desktop apps.

This space will be populated as I use XDG more.

## References

  - [XDG Base Directory Specification](//specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)
