---
title: "Other"
date: 2021-05-03T23:29:57-05:00
draft: false
---

There are a few other things in my life that I can't really categorize under the other sections in my blog.  As I do other random things, those will appear here.

## [Eleni's Website](./elenis-website)

My sister, Eleni, created a really cool website.
