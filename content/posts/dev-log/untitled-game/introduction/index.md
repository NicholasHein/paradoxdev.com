---
title: Introduction
author: Nicholas Hein
date: 2020-12-29T16:47:09-06:00
description: An introduction to the untitled-game project
type: posts
tags:
  - C
  - application
  - game
  - programming
categories:
  - Untitled Game
  - Planning
series:
  - Untitled-Game
draft: false
---

I've been working on **Untitled Game** for around a year, now.  Ever since I was in high school, I've been interested in game programming.  Specifically, I've always enjoyed the Legend of Zelda series.  Then, my senior year of high school, two friends and I decided to create a game for a national competition.  The theme was "80's style arcade," and we wrote a space-shooter game called [Space Traveler](https://www.sites.google.com/site/openoceansoftware/space-traveler).  It qualified us for the national competition in Los Angeles, CA, and it really sparked our interests in creating games together.  Now, as seniors in college, we finally decided it was time to create another one.  However, since our skills were more advanced, we had to come up with a more challenging idea.
<!--more-->

[Repository Link](https://gitlab.com/NicholasHein/untitled-game)

# The Plan

Again, we wanted to make an 80's arcade-style game, but instead of a 3rd-person shooter, we wanted to make an RPG.  In order to keep the project consistent over time, I figured it was important to set a few important principles.  These will help steer the project as development continues.  I called this the *Manifesto*, and it is in the repository at `./game/docs/Manifesto.md`.  At a high level, they're listed here:

  - Fun to play *and* design
  - A 2D retro-style role-playing game
  - Follow the golden rule ([Bushnell's Law](https://en.wikipedia.org/wiki/Bushnell%27s_Law))
  - Technologically advanced but not *modern*

But thus far, we had nothing to make our game really stand out.  Our goal was not to create just another new game.  We wanted to do something radically different.  So, we came up with a few ideas.

  - **Multiple perspectives:** Most retro games were limited to picking one of the typical three perspectives: side-scroller, top-down, and FPV.  The main reason behind this had to do with hardware limitations at the time.  However, with modern GPU, memory, and processor capabilities it's possible to create a game that can fluidly change between the three based on the current level.  Our idea was to create a theme for each perspective.  Top-down might be used when exploring the open world.  Side-scroller might be used for specific spaces and mini-games.  And, FPV could be used for driving or flying.
  - **Custom console:** A more challenging idea we had was to make a custom-built console to play any variety of retro games on.  We could create a console similar to the Nintendo Gameboy such that it's handheld and has minimal hardware (cost-optimized).  We could also create a console similar to the N64 where users can play with their own controllers in a game displayed on their TV via HDMI.  This would introduce a number of challenges such as how the game is loaded into the console and how to allow arbitrary games to be played on the console while forcing them to be strictly retro-style games.
  - **Crowdsourcing:** The most ambitious and unique idea we came up with was to crowdsource game maintenance.  Much like any open source project, after development, we could accept merge requests.  Certainly this would help improve the game code, but it also introduces a possibly unique idea: an indefinitely growing story.  Merge requests need not be limited to game code changes/additions.  They could also add resources and code to grow the story.  Our tech-savvy userbase could design their own levels and characters -- branching out from our base story-line -- and submit them as merge requests upstream.  If accepted, these mainstream storyline additions could be widely distributed in an optional update.  However, even if these additions are not accepted into the upstream storyline, they could still be freely distributed as DLC and downloaded/installed by individuals.  If our game finds a user-base capable of this, our game could not only extend indefinitely, but it could also branch into many vastly different storylines.

The goal is to keep the code as general as possible so any arbitrary game that follows the core principles could be played on it.  This in mind, I came up with a design.

# The Design

The design followed three main rules: keep the engine and application layer seperate, be as modular as possible, and leave all non-essential code to mods.  The full design documentation containing detailed explanations can be found in the repository at `./game/docs/Design.md`.

The very high-level design only includs the major components in the system.

![High level design](/posts/ug_intro_highlvldes.svg)

The design is layered specifically into two layers: Retro-Engine and Untitled Game.  Both layers are as modular as possible.  In the Retro-Engine, only essential code is included in the core and the rest is included in the subsystems which should be statically compiled into the library.  In the Untitled Game, only essential game code is included in the core, and the rest comes from the Retro-Engine and mods.  The Retro-Engine should be dynamically loaded into the game, and the mods can be either dynamically loaded or statically compiled into the program.

The low-level design goes more into depth on resources included in the two cores as well as different types of subsystems, mods, and resources.  For more detailed explanations of these, check out the design documentation in the repository.

![Low level design](/posts/ug_intro_lowlvldes.svg)

# What Next?

I actually started this project months ago, so I created a working graphics demo (v0.1).  However, looking back at the design, I knew I could do better.  This v0.2 is a redesign of the old code base.  It will involve a lot of rewriting and restructuring.  This refined design should help the effort.

What comes next is formulating an implementation plan.  For this, I will use the issue tracker in the repository.  I will then begin implementation according to these issues.  v0.2 is supposed to deliver a basic demo of the major systems.  So, I will document the process on this website, and links to future articles will show up below.
