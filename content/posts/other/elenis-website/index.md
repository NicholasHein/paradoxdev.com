---
title: "Eleni's Website"
author: Nicholas Hein
date: 2021-05-03T23:34:18-05:00
description: My sister, Eleni, created a really cool website
type: posts
tags:
  - family
  - web
  - featured
categories:
  - Other
  - Eleni
draft: false
---

My sister eleni is a graphic artist that is currently going to school at the University of Wisconsin-Stout. She is incredibly talented, and has done a lot of other cool work that I hope to post on here some day. However, her spring 2021 semester, she took a web design course that she was very unsure about when she started. How is she now? I think the results speak for themselves. Here’s the website she created on her own for her final project.

  - Website: [Post-Impressionism](/eleni-hein)
