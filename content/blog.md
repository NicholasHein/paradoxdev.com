---
title: "Blog"
date: 2020-06-27T13:07:35-05:00
draft: false
---

## [Linux Articles](/posts/linux)

This section contains some of the Linux concepts I find interesting.

Nothing to see here yet.  Please check back later when I add more.



## [Development Articles](/posts/development)

This section contains some of the good development practices I've learned.

Nothing to see here yet.  Please check back later when I add more.



## [Development Logs](/posts/dev-log)

This section contains some of the articles I've written for projects I work on.

- [Untitled Game - Introduction](/posts/dev-log/untitled-game/introduction)



## Running

Nothing to see here yet.  Please check back later when I add more.



## Diving

Nothing to see here yet.  Please check back later when I add more.



## [Other](/posts/other)

This section contains some random other things.

  - [Eleni's Website](/posts/other/elenis-website)

