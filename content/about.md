---
title: "About"
date: 2020-06-27T13:07:12-05:00
draft: false
---

Hello!  My name is Nicholas Hein and I'm an embedded software engineer.  I
especially enjoy firmware programming, open-source software, and Linux.  I
also enjoy distance running, SCUBA, and adventuring.

# My Bio

I went to high school in the small rural town of Reedsville, WI where I quickly
discovered my love of programming.  Since my school didn't offer any computer
science courses, I was purely self-taught.  However, once my skills were
sufficiently advanced, I was able to apply myself by building websites in an
independent study.  In my senior year, my friends and I decided it would be fun
to build a game together and submit it to the Future Business Leaders of America
(FBLA) Game and Simulation Programming competition as representatives of our
school.  So my friends Noah Eckstein (musician), James Zahorik (graphic
designer), and I set out to build an 80's arcade game from scratch.  Built in C#
with only Windows Forms and the system libraries, our game featured original
music and graphics. It was a hit.  We ended up qualifying for the national
competition in Anaheim, California where we presented our game to a panel of
judges whilst competing against hundreds of other schools across the country.
This amazing experience really cemented my decision to become a software
engineer.

From that point, I decided to attend the University of Wisconsin-Platteville to
study software engineering.  My freshman year, I was hired as a student Web
Developer for the campus's web development office.  There, I had the amazing
opportunity to learn a lot of industry practices and tools.  I was also exposed
to Linux for the first time.  From there, I began trying different Linux
distributions, and I started learning common Unix command-line tools.  I then
began writing applications for Linux devices.  Soon after, I began reading and
researching the Linux kernel source code.  When I had learned enough to start,
I began modifying and adding to the Linux source code for my embedded projects.

Due to my extremely diverse experiences, I have had the chance to learn
everything from front-end web development to kernel programming to computer
architecture and design.  However, over time, my interests and experiences have
caused me to specialize in kernel/firmware programming.

By the start of this website, I have worked on low-level, cost-minimal, small
form-factor tools, and I have engineered on high-power, advanced, real-time
drone systems.
