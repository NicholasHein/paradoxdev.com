---
title: "Resume"
date: 2020-12-21T14:44:50-06:00
draft: false
---

For a PDF hard-copy of my current resume, click [here](/resume.pdf).  A transcription of my resume from markdown appears below.

---

# Nicholas R. Hein

135 S Court St.; Platteville, WI 53818 | [920.323.5113](tel:9203235113) |
[heinn@uwplatt.edu](mailto:heinn@uwplatt.edu) | [paradoxdev.com](/)



## Education
### Bachelor of Science | University of Wisconsin-Platteville (May 2021)
  - Majors: Software Engineering (ABET Accredited), Mathematics
  - Three-time recipient of William and Jean Sanders scholarship in math
  - Cumulative GPA: 3.365 / 4.0

### High School Diploma | Reedsville High School (May 2017)
  - Graduated with honors; AP Scholar with Distinction
  - Cumulative GPA: 3.739 / 4.0



## Skills & Abilities
### Development
  - **Environments:** Proficient in Linux/Unix environments with GIT-driven CI/CD
  - **Hardware:** Accustomed to many kinds of workstations, servers, embedded systems, and hardware tools
  - **Languages:** Fluent in C, shell scripts, C#, Java, and PHP with experience in assembly languages, C++, and Ruby
  - **Concepts:** Experienced in containerization, emulation, virtualization, and DevOps
  - **Automation:** Familiar with automated software integration, building, testing, packaging, and deployment

### Fields
  - **Low-Level:** Versed in circuit design, computer architecture, and firmware development
  - **Kernel-Level:** Educated in kernel development and network stacks
  - **Application-Level:** Knowledgeable in ABIs, APIs, blockchain, machine learning, databases, and web servers/applications



## Accomplishments
  - **Research:** Began an independent research project focused on creating a decentralized network-layer protocol for the Linux kernel removing the need for ISPs
  - **Competition:** Qualified for the 2017 National FBLA "Game and Simulation Programming" competition in California
  - **Work:** Led full-stack web projects including a staff directory, an interactive campus map, a profile editing tool, a donation system, a course listing web portal, and general web back-end endpoint construction
  - **Projects:** Engineering independent projects including a drone with open-source hardware and software, an open-source retro game engine (written in C), a personally-hosted server, and an 80's arcade game
  - **Contracting:** Created websites for my school district and a local flooring company



## Organizations
  - UW-Platteville Men's Cross Country and Track (2017-Present)
    - Wisconsin Intercollegiate Athletic Conference Scholar (3 years)
    - Wisconsin Intercollegiate Athletic Conference Sportsmanship Award recipient (2 years)
  - Pioneer Student Athletic Advisory Committee (2018-Present)
    - Leadership role with community service emphasis


## Experience
### Web Development Intern | Ariens Co. (May 2020 - Aug. 2020)
  - Worked with engineering, marketing, and administrative needs simultaneously to produce final products
  - Wrote back/front-end for web applications in a business environment

### Aviation Embedded Graphics Intern | Garmin Intl. (May 2020)
  - Hired in September 2019 to write kernel code to support multi-GPU functionality in aviation display units
  - Internship was cancelled due to the COVID-19 pandemic

### Student Web Developer Level 3 | UW-Platteville (Sept. 2017 - May 2020)
  - Collaborated with a team to develop full-stack web applications
  - Customer service involved helping end-users troubleshoot and compile requirements for projects

---
