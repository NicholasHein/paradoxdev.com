#!/bin/bash

set -e

NONASCII=$(find $1 -name '*' | grep -P '[\x80-\xFF]')
while [ -n "$NONASCII" ]
do
	file=`echo "${NONASCII[@]}" | head -1`
	new_name=$(echo "$file" | iconv -f utf8 -t ascii//TRANSLIT)
	echo "Renaming bad path '$file' to '$new_name'"
	mv "$file" "$new_name"
	NONASCII=$(find $1 -name '*' | grep -P '[\x80-\xFF]')
done

