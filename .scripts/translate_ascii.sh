#!/bin/bash

set -e

# $ ./.scripts/translate_ascii.sh "./static/eleni-hein" "*.html"
# >>>>> find ./static/eleni-hein -name "*.html" -exec iconv -f utf8 -t ascii/TRANSLIT {} > {} \;
find "$1" -name "$2" -exec iconv -f utf8 -t ascii//TRANSLIT {} -o {} \; 
